#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>

/*
 *              Unit
 *      Archer          LaserCannon
 *                  Army
 */
struct Unit {
    virtual int strength() = 0;
    virtual std::shared_ptr<Unit> getComposite() {
        return nullptr;
    }
};

struct CompositeUnit : public Unit,
                       public std::enable_shared_from_this<CompositeUnit> {
public:
    int strength() {
        return std::accumulate(
            units.begin(), units.end(), 0,
            [](int init, auto&& u) { return init + u->strength(); });
    }

    std::shared_ptr<Unit> getComposite() override {
        return shared_from_this();
    }

    bool removeUnit(std::shared_ptr<Unit> u) {
        auto iter = find(units.begin(), units.end(), u);
        if (iter == units.end()) {
            return false;
        }
        units.erase(iter);
        return true;
    }

    bool addUnit(std::shared_ptr<Unit> u) {
        try {
            units.push_back(u);
        } catch (...) {
            return false;
        }
        return true;
    }

    static std::shared_ptr<CompositeUnit> newInstance() {
        throw "xxxx";
    }

private:
    std::vector<std::shared_ptr<Unit>> units;
};

class Archer : public Unit {
public:
    int strength() override {
        return 44;
    }
};

struct LaserCannon : public Unit {
public:
    int strength() override {
        return 10;
    }
};

class Army : public CompositeUnit {
public:
    static std::shared_ptr<Army> newInstance() {
        return std::make_shared<Army>();
    }
    virtual ~Army() noexcept {}
};

int main() {
    std::shared_ptr<Army> a = Army::newInstance();
    std::shared_ptr<Army> b = Army::newInstance();

    a->addUnit(std::make_shared<Archer>());
    a->addUnit(std::make_shared<LaserCannon>());
    b->addUnit(std::make_shared<Archer>());

    a->addUnit(b);

    std::cout << a->strength() << std::endl;

    if (std::shared_ptr<Unit> cm = a->getComposite()) {
        std::cout << "xxx: " << cm->strength() << std::endl;
    }
}

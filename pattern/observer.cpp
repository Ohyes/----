#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <cassert>

class Observer;
class Observable {
public:
    virtual void attach(std::shared_ptr<Observer> ob) = 0;
    virtual void detach(std::shared_ptr<Observer> ob) = 0;
    virtual void notify() = 0;
};

class Observer {
public:
    virtual void update(std::shared_ptr<Observable> ob) = 0;
};

class Login : public Observable,
              public std::enable_shared_from_this<Login> {
public:
    void attach(std::shared_ptr<Observer> ob) override {
        observers.push_back(ob);
    }

    void detach(std::shared_ptr<Observer> ob) override {
        auto iter = std::find(observers.begin(), observers.end(), ob);
        assert(iter != observers.end());
        observers.erase(iter);
    }

    void notify() override {
        for (auto&& ob : observers) {
            ob->update(shared_from_this());
        }
    }

    bool handleLogin(std::string user, std::string pass) {
        // login...
        notify();
        return true;
    }

    int getStatus() {
        return 1;  // stands for wrong password
    }

    static std::shared_ptr<Login> getInstance() {
        std::shared_ptr<Login> ptr(new Login);
        return ptr;
    }

private:
    Login() = default;

    std::vector<std::shared_ptr<Observer>> observers;
};

class LoginObserver : public Observer,
                      public std::enable_shared_from_this<LoginObserver> {
public:
    LoginObserver(std::shared_ptr<Login> l) : login(l) {}

    void init() {
        login->attach(shared_from_this());
    }

    void update(std::shared_ptr<Observable> o) override {
        if (o == login) {
            auto ptr = std::dynamic_pointer_cast<Login>(o);
            doUpdate(ptr);
        }
    }

private:
    virtual void doUpdate(std::shared_ptr<Login> l) = 0;

    std::shared_ptr<Login> login;
};


// some checks
class SecurityMonitor : public LoginObserver {
public:
    using LoginObserver::LoginObserver;

private:
    void doUpdate(std::shared_ptr<Login> l) override {
        int st = l->getStatus();
        if (st == 1) {
            std::cout << "send mail to administrator\n";
        }
    }
};

class GeneralLogger : public LoginObserver {
public:
    using LoginObserver::LoginObserver;

private:
    void doUpdate(std::shared_ptr<Login> l) override {
        std::cout << "log it\n";
    }
};

class PartnershipTool : public LoginObserver {
public:
    using LoginObserver::LoginObserver;

private:
    void doUpdate(std::shared_ptr<Login> l) override {
        int st = l->getStatus();
        std::cout << "PartnershipTool\n";
    }
};

int main() {
    auto login_ptr = Login::getInstance();
    std::make_shared<SecurityMonitor>(login_ptr)->init();
    std::make_shared<GeneralLogger>(login_ptr)->init();
    std::make_shared<PartnershipTool>(login_ptr)->init();
    login_ptr->handleLogin("bob", "xxx");
    return 0;
}

#include <iostream>
#include <string>
#include <memory>
#include <utility>
#include <regex>

class Marker {
public:
    Marker(std::string s) : text(std::move(s)) {}

    virtual bool mark(std::string input) = 0;

protected:
    std::string text;
};

class MarkLogicMarker : public Marker {
public:
    using Marker::Marker;

    bool mark(std::string input) override {
        return true;
    }
};

class MatchMarker : public Marker {
public:
    using Marker::Marker;

    bool mark(std::string input) override {
        return text == input;
    }
};

class RegexMarker : public Marker {
public:
    using Marker::Marker;

    bool mark(std::string input) override {
        std::regex reg(text);
        return  std::regex_match(input, reg);
    }
};

class Question {
public:
    Question(std::shared_ptr<Marker> marker) : marker(std::move(marker)) {}

    bool mark(std::string input) {
        return marker->mark(std::move(input));
    }

private:
    std::shared_ptr<Marker> marker;
};

class TextQuestion : public Question {
public:
    TextQuestion(std::string s)
        : Question(std::make_shared<MarkLogicMarker>(std::move(s))) {}
};

class XXXQuestion : public Question {
public:
    XXXQuestion(std::string s)
        : Question(std::make_shared<MatchMarker>(std::move(s))) {}
};

int main() {
    TextQuestion q1("fdjfkdjfkdfj");
    XXXQuestion q2("quest");

    boolalpha(std::cout);
    std::cout << q1.mark("fdjkfjdkfjd") << '\n';
    std::cout << q2.mark("make") << '\n'
              << q2.mark("quest") << '\n';
    return 0;
}

#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

class AbstractDispatcher;
class Unit {
public:
    virtual std::shared_ptr<Unit> getComposite() {
        return {};
    }

    virtual void accept(std::shared_ptr<AbstractDispatcher> dispatcher) = 0;

    virtual int bombardStrength() = 0;
};

class CompositeUnit : public Unit,
                      public std::enable_shared_from_this<CompositeUnit> {
public:
    std::shared_ptr<Unit> getComposite() override {
        return shared_from_this();
    }

    void removeUnit(std::shared_ptr<Unit> u) {
        auto iter = std::find(units.begin(), units.end(), u);
        units.erase(iter);
    }

    void addUnit(std::shared_ptr<Unit> u) {
        auto iter = std::find(units.begin(), units.end(), u);
        if (iter != units.end())
            return;
        units.push_back(u);
    }

protected:
    std::vector<std::shared_ptr<Unit>> units;
};

class Archer;
class LaserCannonUnit;
class Army;
class AbstractDispatcher {
public:
    virtual void dispatch(std::shared_ptr<Archer> obj) = 0;
    virtual void dispatch(std::shared_ptr<LaserCannonUnit> obj) = 0;
    virtual void dispatch(std::shared_ptr<Army> obj) = 0;
};

class Army : public CompositeUnit {
public:
    int bombardStrength() override {
        return std::accumulate(units.begin(), units.end(), 0,
                               [](int init, std::shared_ptr<Unit> u) {
                                   return init + u->bombardStrength();
                               });
    }

    void accept(std::shared_ptr<AbstractDispatcher> dispatcher) override {
        auto self = shared_from_this();
        dispatcher->dispatch(std::dynamic_pointer_cast<Army>(self));
    }
};

class LaserCannonUnit : public Unit,
                        public std::enable_shared_from_this<LaserCannonUnit> {
public:
    int bombardStrength() override {
        return 42;
    }

    void accept(std::shared_ptr<AbstractDispatcher> dispatcher) override {
        dispatcher->dispatch(shared_from_this());
    }
};

class Archer : public Unit, public std::enable_shared_from_this<Archer> {
public:
    int bombardStrength() override {
        return 12;
    }

    void accept(std::shared_ptr<AbstractDispatcher> dispatcher) override {
        dispatcher->dispatch(shared_from_this());
    }
};

class Dispatcher : public AbstractDispatcher {
public:
    void dispatch(std::shared_ptr<Archer> obj) override {
        std::cout << "dispatching Archer obj\n";
    }

    void dispatch(std::shared_ptr<LaserCannonUnit> obj) override {
        std::cout << "dispatching LaserCannonUnit obj\n";
    }

    void dispatch(std::shared_ptr<Army> obj) override {
        std::cout << "dispatching Army obj\n";
    }
};

int main() {
    auto dispatcher = std::make_shared<Dispatcher>();
    std::vector<std::shared_ptr<Unit>> units = {
        std::make_shared<Archer>(),
        std::make_shared<LaserCannonUnit>(),
        std::make_shared<Army>()
    };
    for (auto&& u : units) {
        u->accept(dispatcher);
    }
    return 0;
}

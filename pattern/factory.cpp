#include <iostream>
#include <string>
#include <memory>

struct Stream {
};

struct Encoder {
    virtual std::shared_ptr<Stream> encode() = 0;
};

struct MusicEncoder : public Encoder {
    std::shared_ptr<Stream> encode() {

    }
};

struct TextEncoder : public Encoder {
    std::shared_ptr<Stream> encode() {
    }
};

struct EncoderManager {
    static MusicEncoder getMusicEncoder() {
    }
    static TextEncoder getTextEncoder() {
    }
};

int main() {
}

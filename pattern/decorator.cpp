#include <iostream>
#include <memory>

class RequestHelper {
};

class ProcessRequest {
public:
    virtual int process(RequestHelper rh) = 0;
};

class MainProcess : public ProcessRequest {
public:
    int process(RequestHelper rh) override {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        return 0;
    }
};

class DecoratorProcess : public ProcessRequest {
public:
    DecoratorProcess(std::shared_ptr<ProcessRequest> pr) : pr(pr) {}

protected:
    std::shared_ptr<ProcessRequest> pr;
};

class LogRequest : public DecoratorProcess {
public:
    using DecoratorProcess::DecoratorProcess;
    int process(RequestHelper rh) override {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        pr->process(rh);
        return 0;
    }
};

class AuthenticateRequest : public DecoratorProcess {
public:
    using DecoratorProcess::DecoratorProcess;
    int process(RequestHelper rh) override {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        pr->process(rh);
        return 0;
    }
};

class StructureRequest : public DecoratorProcess {
public:
    using DecoratorProcess::DecoratorProcess;
    int process(RequestHelper rh) override {
        std::cout << __PRETTY_FUNCTION__ << '\n';
        pr->process(rh);
        return 0;
    }
};

int main() {
    auto process =
        std::make_shared<AuthenticateRequest>(
            std::make_shared<StructureRequest>(
                std::make_shared<LogRequest>(
                    std::make_shared<MainProcess>())));
    process->process(RequestHelper());
}

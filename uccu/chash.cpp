#include <iostream>
#include <algorithm>
#include <string>
#include <cstdint>
#include <vector>
#include <map>

uint32_t hash(const std::string& s) {
    uint32_t r = 0;
    for (auto&& c : s) {
        r = 31 * r + (c - ' ');
    }
    return r;
}

class Server {
public:
    Server() = default;

    // 查看数据放在哪台主机上
    // 采用一致性哈希
    std::string getMachineByObject(std::string name) {
        uint32_t h = hash(name);
        auto iter = pos.lower_bound(h);
        if (iter == pos.end()) {
            return pos.begin()->second;
        }
        return iter->second;
    }

    void addMachine(std::string name) {
        hosts.push_back(name);
        pos[hash(name)] = name;
    }

    void delMachine(std::string name) {
        auto iter = find(begin(hosts), end(hosts), name);
        hosts.erase(iter);
        uint32_t h = hash(name);
        pos.erase(h);
    }

private:
    std::vector<std::string> hosts;
    std::map<uint32_t, std::string> pos;
};

int main() {

}
